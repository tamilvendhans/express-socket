import firebase from 'firebase/app';
import "firebase/auth"
import { FB_CONFIG } from "../configs/fbConfig";

firebase.initializeApp(FB_CONFIG);


export default firebase;
