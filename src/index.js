import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import routesConfig from './configs/routes';
import CommonRoute from './components/CommonRoute';
import PageNotFound from './components/PageNotFound/PageNotFound';
import firebase from './services/firebase'

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'react-toastify/dist/ReactToastify.min.css'
import './index.css';



const App = () => {

  const [isLoading, setIsLoading] = React.useState(false)

  return (
    <React.StrictMode>
      <div id="loadingGuage" className={isLoading ? '' : 'hide'}><span class="fa fa-spinner fa-5x fa-pulse text-white"></span></div>
      <Router>
        <Switch>
          {
            routesConfig.map((item, index) => (
              <CommonRoute {...item} toggleLoadingGuage={setIsLoading} key={`Route_${index}_${item.label}`.toString()} />
            ))
          }          
          <CommonRoute path="**" component={PageNotFound} />
        </Switch>
      </Router>
    </React.StrictMode>
  );
}

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);