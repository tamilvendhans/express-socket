import Chats from "../components/Chats/Chats";
import ChatLayout from "../components/Layouts/ChatLayout";
import Login from "../components/Login/Login";

const routesConfig = [
    {
        path: "/",
        label: "Chats",
        layout: ChatLayout,
        component: Chats,
        exact: true
    },
    {
        path: "/login",
        label: "Login",
        component: Login
    }
]

export default routesConfig;