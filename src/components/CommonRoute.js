import React from 'react'
import { Route } from 'react-router-dom'

const CommonRoute = ({component: Component, layout: Layout, ...rest}) => {
    return (
        <Route
            {...rest}
            render={(props) => {
                return (
                    <>
                    {Layout ? 
                        <Layout>
                            <Component {...props} />
                        </Layout> 
                        : <Component {...props} />
                    }
                    </>
                )
            }}
        />
    )
}

export default CommonRoute;
