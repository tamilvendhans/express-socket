import React, { Component } from 'react';
import './login.css';
import firebase from '../../services/firebase';

export class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            confirmationResult: null,
            isCodeComplete: false,
            isFinishOnlyMode: false
        }
    }

    setUpRecaptcha = () => {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
            size: 'invisible',
            callback: (response) => {
              // reCAPTCHA solved, allow signInWithPhoneNumber.
              this.onSignInSubmit();
            }
        });
    }

    onSignInSubmit = (event) => {
        event.preventDefault();
        this.setUpRecaptcha()
        const phoneNumber = '+91' + event.target.phone.value;
        const appVerifier = window.recaptchaVerifier;
        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then((confirmationResult) => {
            
                this.setState({confirmationResult: confirmationResult});
            
            }).catch((error) => {
            
            });
    }

    onCodeSubmit = (event) => {
        event.preventDefault();
        const code = event.target.code.value;
        this.state.confirmationResult.confirm(code).then((result) => {
            if(result.user) {
                this.setState({isCodeComplete: true})
            }
        }).catch((error) => {

        });
    }

    onFinishSubmit = (event) => {
        event.preventDefault();
    }    

    render() {
        const { confirmationResult, isCodeComplete } = this.state;

        return (
            <div className="m-auto col-md-6 col-sm-12 p-lg-4 p-sm-3 p-3">
                <div className="text-center">
                    <img src="chaticon.png" alt="chat image" className="img img-thumbnail" />
                </div>
                <h1 className="text-center my-2" id="title">Login</h1>
                <PhoneForm onSignInSubmit={this.onSignInSubmit} />
                <CodeConfirmForm onCodeSubmit={this.onCodeSubmit} />
                <FinishForm onFinishSubmit={this.onFinishSubmit} />
            </div>
        )
    }
}

const PhoneForm = (props) => {
    return (
        <form id="phoneForm" onSubmit={props.onSignInSubmit} className="form my-5" autoComplete="off">
            <div id="recaptcha-container"></div>
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                <span className="input-group-text">+91</span>
                </div>
                <input type="number" maxLength="10" required autoFocus className="form-control" name="phone" placeholder="Phone number" />
                <span className="invalid-feedback d-block" id="phoneError"></span>
            </div>
            <div className="text-right">
                <button type="submit" className="sign-in-button btn btn-primary">Sign In</button>
            </div>
            <div className="clearfix"></div>
        </form>
    )
}

const CodeConfirmForm = (props) => {
    return (
        <form id="codeForm" onSubmit={props.onCodeSubmit} className="form my-5" autoComplete="off">
            <input type="number" className="form-control mb-3" name="code" maxLength="6" required placeholder="Enter the code" />
            <div className="text-right">
                <button type="submit" className="btn btn-primary">Confirm Code</button>
            </div>
            <div className="mt-4">
                <span className="fl text-primary" id="codeResend">Re-send the code</span>
                <span className="fr text-primary" id="mobileReenter">Re-enter the phone number</span>
            </div>
            <div className="clearfix"></div>
        </form>
    )
}

const FinishForm = (props) => {
    return (
        <form id="profileCompleteForm" onSubmit={props.onFinishSubmit} className="form">
            <div className="text-center"><span className="text-light" id="curProfileId"></span></div>
            <div className="form-group my-3">
                <label for="dispName">Display Name</label>
                <input type="text" className="form-control" id="dispName" name="dispName" max="25" min="3" required />
            </div>
            <div className="form-group my-3">
                <label for="dob">Date of Birth</label>
                <input type="date"  className="form-control" id="dob" name="dob" required />
            </div>
            <div className="text-right">
                <button type="submit" className="btn btn-primary">Complete Registration</button>
            </div>
            <div className="text-center py-2 mt-3 border-top border-bottom"><span>(or)</span></div>
            <div className="mt-4 text-center">
                <span className="text-primary" id="signOut">Sign Out</span> and try with another account.
            </div>
        </form>
    )
}

export default Login

