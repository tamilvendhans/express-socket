$("#loadingGuage").removeClass("hide");
var fbConfig = {
    apiKey: "AIzaSyAhZ2or37fRxUWZkIN317ek7vwoZj16gtM",
    authDomain: "friends-chat-d08ee.firebaseapp.com",
    databaseURL: "https://friends-chat-d08ee.firebaseio.com",
    projectId: "friends-chat-d08ee",
    storageBucket: "friends-chat-d08ee.appspot.com",
    messagingSenderId: "961888947519",
    appId: "1:961888947519:web:77dfffe07e8b522cbe578a"
};

if(firebase.apps.length == 0) { 
    firebase.initializeApp(fbConfig);
}

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        window.displayName = user.displayName && user.displayName != null ? user.displayName : '';
        window.uid = user.uid;
        if(!displayName && !uid) {
            window.location.pathname = '/'
        }
    } else {
        window.location.pathname = '/'
    }
});

if(firebase.auth().currentUser) {
    let user = firebase.auth().currentUser;
    window.displayName = user.displayName && user.displayName != null ? user.displayName : '';
    window.uid = user.uid;
}