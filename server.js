const express = require('express');
const app = express();
const http = require('http');
const path = require('path');
const server = http.createServer(app);
const { Server } = require("socket.io");
const chatRouter = require('./chatRoutes');
const exphbs = require('express-handlebars');
const io = new Server(server);
require('dotenv').config();

app.engine('html', exphbs({
    extname: '.html',
    defaultLayout: 'main',
    partialsDir: path.join(__dirname, 'views')
}));
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public', 'css')));
app.use(express.static(path.join(__dirname, 'public', 'assets')));
app.use(express.static(path.join(__dirname, 'public', 'js')));
// app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'build')));

// app.get('/login', (req, res) => {
//     res.render('login', {title: "Home | Chat Login", layout: false});
// });

// app.get('/chats', (req, res) => {
//     res.render('chats', {title: "Your Friends"});
// });

// app.get('/chat/:id', (req, res) => {
//   res.render('chat', {title: "Chat"});
// });

app.use("/api/v1", chatRouter)

io.on('connection', (socket) => {
    socket.on('chat message', (msg, name) => {
        io.emit('chat message', msg, name);
    });
    socket.on("private message", ({ content, to }) => {
        socket.to(to).emit("private message", {
          content,
          from: socket.id,
        });
    });
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, "build", "index.html"))
});

server.listen(process.env.PORT || 3000, () => {
    console.log(`listening on *:${process.env.PORT || 3000}`);
});